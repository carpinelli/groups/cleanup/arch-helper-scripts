#! /usr/bin/env python3

"""Moves the mouse around every 30 seconds to keep the computer
awake."""

import sys
import time
import random

import pyautogui


def failsafe_max() -> tuple:
    """Returns a tuple containing the x and y max for the current
    pyautogui.FAILSAFE_POINTS."""
    max_x = 0
    max_y = 0
    for coordinate in pyautogui.FAILSAFE_POINTS:
        x, y = coordinate
        if x > max_x:
            max_x = x
        if y > max_y:
            max_y = y
    return (max_x, max_y)


def will_failsafe_trigger(Movement_Amount: tuple) -> bool:
    """If moving the mouse by Movement_Amount will set off a
    pyautogui.FAILSAFE check, this function returns True. If not, this
    function returns False."""
    Failsafe_Max = failsafe_max()
    Position = pyautogui.position()
    Position_X = Position[0]
    Position_Y = Position[1]

    # Check if moving mouse Movement_Amount will put the mouse in an
    # area of the screen that will trigger PyAutoGUI's FAILSAFE exception
    flag_x = ((Position_X + Movement_Amount[0]) < 1
              or (Position_X + Movement_Amount[0]) >= Failsafe_Max[0])
    flag_y = ((Position_Y + Movement_Amount[1]) < 1
              or (Position_Y + Movement_Amount[1]) >= Failsafe_Max[1])

    if flag_x and flag_y:
        return True
    else:
        return False


def main() -> None:
    Minute = 60  # Seconds
    Amount_Range = (-500, 500)
    Drag_Duration = 0.54

    if len(sys.argv) > 1:
        Wait = float(sys.argv[1])
    else:
        Wait = Minute * 9

    while True:
        time.sleep(Wait)
        while True:
            amount = (random.randint(Amount_Range[0], Amount_Range[1]),
                      random.randint(Amount_Range[0], Amount_Range[1]))
            if not will_failsafe_trigger(amount):
                break
        pyautogui.moveRel(amount, duration=Drag_Duration)

    return None


if __name__ == "__main__":
    main()
